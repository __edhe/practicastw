document.addEventListener('DOMContentLoaded', function (){
    //Animaciones de inicio
    document.getElementById('cont-form').animate([
        {transform: 'scale(.5)'},
        {transform: 'translateY(-10px)'},
        {transform: 'translateY(0px)'},
        {transform: 'scale(1)'}
    ],{duration: 500});
    document.getElementById('login').animate([{opacity: '0'},{opacity: '1'}],{duration: 1000}); 
    //Reestablecer msg
    let timeout; //Desaparecer label al escribir
    document.getElementById('password').addEventListener('keyup', function() {
        clearTimeout(timeout);
        timeout = setTimeout(() => {
            msg.style.display = "none";      
            clearTimeout(timeout)
        }, 1000)
    });

    document.getElementById('btn-singup').addEventListener('click', function() {
        setAnimacion(1);
    });
    document.getElementById('btn-login').addEventListener('click', function() {
        setAnimacion(2);
    });
    document.getElementById('singup-form').addEventListener('submit', validarRegistro);
    document.getElementById('login-form').addEventListener('submit', validarIngreso);
});

async function validarRegistro(ev){
    ev.preventDefault();
    console.log("Se recibió el formulario de registro");
    const nombre = document.getElementById('nombre').value;
    const usuario = document.getElementById('usuario').value;
    const apellido = document.getElementById('apellido').value;
    const tipo = document.getElementById('tipo').value;
    const pais = document.getElementById('pais').value;
    const email = document.getElementById('email').value;
    const password = document.getElementById('password').value;
    const tarjeta = document.getElementById('tarjeta').value;
    let re_password = document.getElementById('re-password').value;
    

    console.log("nombre: " + nombre);
    console.log('apellido: '+apellido);
    console.log("email: " + email);
    console.log("password: " + password);
    console.log("re-password: " + re_password);
    console.log("tarjeta: " +tarjeta);
    console.log("usuario: " + usuario);
    console.log("tipo: " + tipo);
    console.log("pais: " + pais);

    var msg = document.getElementById('msg');    
    if ( password.length >= 8 ) 
        if ( password == re_password ) {
            console.log("Las contraseñas coinciden\nAcceso"); 
            setAnimacion(1);
            alert("Contraseña correctas");


            $.ajax({
                type: 'post',
                url: 'get_form.php',
                async: true,
                data: {
                    nombre: nombre, email: email, password: password, usuario: usuario, tipo: tipo, pais: pais, apellido: apellido, tarjeta: tarjeta
                },
                success: function(result){
                    console.log(result);
                }
            });


        } else {
            console.log("Las contraseñas no coinciden\nDenegado");
            msg.textContent = "Las contraseñas no coinciden";
            msg.style.display = "block"; 
            return false;
        }
    else {
        console.log("La contraseña es demasiado corta");
        msg.textContent = "Contraseña corta";
        msg.style.display = "block";      
        return false;
    }
};
function validarIngreso(ev) {
    ev.preventDefault();
    console.log("Se recibió el formulario de registro");
    const email = document.getElementById('email_').value;
    const password = document.getElementById('password_').value;

    console.log("email: " + email);
    console.log("password: " + password);
    //Que poco seguro validar con JS :c
    
    $.ajax({
        type: 'post',
        url: 'get_form_.php',
        async: true,
        data: {
            email: email, password: password
        },
        success: function(result){
            console.log(result);
            if (result == 'true'){
                console.log("Redireccion");
                let targetURL = 'perfil.php';
                let newURL = document.createElement('a');
                newURL.href = targetURL;
                document.body.appendChild(newURL);
                newURL.click();
            }
        }
    });
    // if (result == true) {
    //     alert("Bienvenido");
    // } else {
    //     alert("Usuario no registrado");
    // }
}