<?php
    include_once __DIR__.'/database.php';
    $data = array();
    $eliminado = 0;
    $f = false;
    // SE OBTIENE LA INFORMACIÓN DEL PRODUCTO ENVIADA POR EL CLIENTE
    $producto = file_get_contents('php://input');
    if(!empty($producto)) {
        // SE TRANSFORMA EL STRING DEL JASON A OBJETO
        $jsonOBJ = json_decode($producto);
        /**
         * SUSTITUYE LA SIGUIENTE LÍNEA POR EL CÓDIGO QUE REALICE
         * LA INSERCIÓN A LA BASE DE DATOS. COMO RESPUESTA REGRESA
         * UN MENSAJE DE ÉXITO O DE ERROR, SEGÚN SEA EL CASO.
         */
        $name = $jsonOBJ->nombre;
        echo '[SERVIDOR] Nombre: '.$jsonOBJ->nombre. "\n";
        echo '[SERVIDOR] Marca: '.$jsonOBJ->marca . "\n";
        echo '[SERVIDOR] Modelo: '.$jsonOBJ->modelo . "\n";
        echo '[SERVIDOR] Precio: ' . $jsonOBJ->precio . "\n";
        echo '[SERVIDOR] Detalles: '.$jsonOBJ->detalles . "\n";
        echo '[SERVIDOR] Unidades: '.$jsonOBJ->nombre . "\n";
        echo '[SERVIDOR] Imagen: '.$jsonOBJ->imagen . "\n";

        $data = array();
        
            
        // echo $eliminado;
        /** SE CREA EL OBJETO DE CONEXION */
        @$link = new mysqli('localhost', 'root', 'P@$$w0rd', 'marketzone');	

        /** comprobar la conexión */
        if ($link->connect_errno) 
        {
            die('Falló la conexión: '.$link->connect_error.'<br/>');
                /** NOTA: con @ se suprime el Warning para gestionar el error por medio de código */
        }
        
        /** Crear una tabla que no devuelve un conjunto de resultados */
        if ( $result = $link->query("SELECT nombre,eliminado FROM productos WHERE nombre = '{$name}' AND eliminado = '{$eliminado}' ") )
        {
            
            $row = $result->fetch_all(MYSQLI_ASSOC);
            if (!is_null($row)) {
                /** útil para liberar memoria asociada a un resultado con demasiada información */
                foreach($row as $num => $registro) {            // Se recorren tuplas
                    foreach($registro as $key => $value) {      // Se recorren campos
                        $data[$num][$key] = $value;
                    }
                }
            } else {
                echo "No se encontraron registros";
            }
            $result->free();
        }
        // echo json_encode($data, JSON_PRETTY_PRINT);
    }

    foreach($row as $num => $registro) {
        echo "Nombre: ".$data[$num]['nombre'];
        echo "\nEliminado: ".$data[$num]['eliminado'];
        $f = true;
    }
    if ($f) {
        echo "\nSe encontraron coincidencias, no se insertará este producto\n";
    } else {
        echo "\nNo se encontraron coincidencias o eliminado = 1\n";
        echo "El producto será insertado\n\n";
        $sql = "INSERT INTO productos VALUES (null, '{$jsonOBJ->nombre}', '{$jsonOBJ->marca}','{$jsonOBJ->modelo}',{$jsonOBJ->precio},'{$jsonOBJ->detalles}', {$jsonOBJ->unidades}, '{$jsonOBJ->imagen}',0)";
        echo $sql ."\n";
        if ($link->query($sql)) {
            echo "Producto insertado con ID: ".$link->insert_id;
        }
        else {
            echo "El producto no pudo ser insertado";
        }
        
    }
    $link->close();
?>