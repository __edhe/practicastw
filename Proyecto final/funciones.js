
function setAnimacion(c){
    //c == 2 Mostrar formulario de registro
    //c == 1 Mostrar formulario de ingreso
    console.log("Transaladar y mostrar formulario " + c);
    let formOcultar, formMostrar;
    if (c == 1 ) {
        formOcultar = document.getElementById('singup-form');
        formMostrar = document.getElementById('login-form');
    } else {
        formOcultar = document.getElementById('login-form');
        formMostrar = document.getElementById('singup-form');
    }
    transladar(formOcultar,formMostrar, c);
}
function transladar(formOcultar, formMostrar, c) {
    let contForm = document.getElementById('cont-form');
    //Determinamos la dirección del transformX
    let origen, destino;
    if ( c == 1) {
        origen = '0';
        destino = '125';
    } else {
        origen = '125';
        destino = '0';
    }
    formOcultar.animate([
        {opacity: '1'},
        {opacity: '0'}
    ], {duration: 1000});
    formOcultar.style.display = "none";
    formMostrar.style.display = "block";
    formMostrar.animate([
        {opacity: '0'},
        {opacity: '1'}
    ], {duration: 1000});
    contForm.animate([
        {transform: 'translateX('+origen+'%)'},
        {transform: 'translateX('+destino+'%)'}
    ], {duration: 500});
    contForm.style.transform = 'translateX('+destino+'%)';
}
function ancla(key){
    switch (key) {
        case 0:
            document.getElementById('acerca').scrollIntoView({behavior: 'smooth'});
            break;
        case 1:
            document.getElementsByClassName('secondary-wrapper')[0].scrollIntoView({behavior: 'smooth'});
            break;
        case 2:
            console.log("Redireccionamiento a catálogo");
            break;
        case 3:
            location.href = "ingresar.php";
            break;
        default:
            break;
    }
}