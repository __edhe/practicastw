<?php 
    session_start();
    $edad = intval($_SESSION['edad']);
    include_once __DIR__.'/database.php';
    $data = array();
    // echo $edad;
    // $edad = 8;

    // Limites 7, 12, 15, 18
    if($edad < 7 ){
        // Solo clasificación A y AA
        $condicion = 'WHERE idClasificacion <= 2';
    } elseif ($edad <= 15) {
        $condicion = 'WHERE idClasificacion <= 4';
    }elseif ($edad >= 18) {
        // Todas las clasificaciones 
        $condicion = 'WHERE idClasificacion <= 6';
    }
    $sql = "SELECT * FROM pelicula ".$condicion;
    // echo $sql;
    // if ($series = $conexion->query($sql));
    if ($result = $conexion->query($sql)){
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        if(!is_null($rows)) {
            foreach($rows as $num => $row) {
                foreach($rows as $key => $value) {
                    $data_[$num][$key] = $value;
                }
            }
        }
        $result->free();
    }else {
        die('Query Error: '.mysqli_error($conexion));
    }
    echo json_encode($data_, JSON_PRETTY_PRINT);
?>