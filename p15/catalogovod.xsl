<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="html" encoding="UTF-8" doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"
        doctype-system="http://www.w3.org/TR/xhtml1/DTD/strict.dtd"/>
    <xsl:template match="/">
        <html>
            <style>
                body {font-family: Arial, Helvetica, sans-serif;}
                
                table {     font-family: "Lucida Sans Unicode", "Lucida Grande", Sans-Serif;
                font-size: 12px;    margin: 45px;     width: 480px; text-align: left;    border-collapse: collapse; }
                
                th {     font-size: 13px;     font-weight: normal;     padding: 8px;     background: #b9c9fe;
                border-top: 4px solid #aabcfe;    border-bottom: 1px solid #fff; color: #039; }
                
                td {    padding: 8px;     background: #e8edff;     border-bottom: 1px solid #fff;
                color: #669;    border-top: 1px solid transparent; }
                
                tr:hover td { background: #d0dafd; color: #339; cursor:pointer}
            </style>
            <head>
                <title>NETFLIX CHIPEADO</title>
            </head>
            <body>
                <table border="1">
                    <tr>
                        <th align="center" colspan="4">Peliculas</th>
                    </tr>
                    <tr>
                        <th align="center">Titulo</th>
                        <th align="center">Género</th>
                        <th align="center">Duración</th>
                    </tr>
                    <xsl:for-each select="CatalogoVOD/contenido">
                        <xsl:for-each select="peliculas/genero/titulo">
                            <tr>
                                <td>
                                    <xsl:value-of select="."/>
                                </td>
                                <td>
                                    <xsl:for-each select="..">
                                        <xsl:value-of select="@nombre"/>
                                    </xsl:for-each>
                                </td>
                                <td>
                                    <xsl:for-each select=".">
                                        <xsl:value-of select="@duracion"/>
                                    </xsl:for-each>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </xsl:for-each>
                </table>
                <br/>
                <table border="1">
                    <tr>
                        <th align="center" colspan="5">Series</th>
                    </tr>
                    <tr>
                        <th align="center">Titulo</th>
                        <th align="center">Género</th>
                        <th align="center">temporada</th>
                    </tr>
                    <xsl:for-each select="CatalogoVOD/contenido">
                        <xsl:for-each select="series/genero/titulo">
                            <tr>
                                <td>
                                    <xsl:value-of select="."/>
                                </td>
                                <td>
                                    <xsl:for-each select="..">
                                        <xsl:value-of select="@nombre"/>
                                    </xsl:for-each>
                                </td>
                                <td>
                                    <xsl:for-each select=".">
                                        <xsl:value-of select="@duracion"/>
                                    </xsl:for-each>
                                </td>
                            </tr>
                        </xsl:for-each>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
