<?php
    use backend\Webtechnologies\API\Productos;
    require_once __DIR__.'/Webtechnologies/API/Productos.php';

    $productos = new Productos('marketzone');
    $productos->search( $_GET['search'] );
    echo $productos->getResponse();
?>