function validation() {
    var send = 0;
    let nombre = document.getElementById('name').value;
    if ( nombre.length <= 100 ) { // nombre
        console.log("1. La longitud del nombre es correcta: (", nombre.length,")");
        send++;
    } else {
        console.log("1. La longitud del nombre exede el permitido (", nombre.length,") máximo: 100 carácteres");
    }

    let marca = document.getElementById("marca").value;
    let marcas = ['Xiaomi', 'Apple', 'Samsung', 'Motorola', 'Huawei'];

    let f = false;
    let c = 0;

    while (c < marcas.length && !f) {
        if (marca == marcas[c]) {
            f = true;
        } else f = false; c++;
    }
    if ( f ) {
        console.log("2. Se seleccionó una marca de la lista: ", marca);
        send++;
    } else console.log("2. No se seleccionó una marca de la lista");
    
    let modelo = document.getElementById('modelo').value;
    if ( modelo.length <= 25 ) {
        console.log("3. Longitud del modelo correcta: ", modelo.length);
        // Comprobar alfanumérico
        send++;
    } else {
        console.log("3. La longitud del modelo es mayor a la  permitida", modelo.length);
    }

    let precio = document.getElementById('precio').value;
    precio = parseFloat(precio);
    if ( precio <= 99.99) {
        console.log("4. El precio es menor a 99.99")
    } else {
        console.log("4. El precio es correcto, mayor a 99.99");
        send++;
    }
    let detalles = document.getElementById('detalles').value;
    if ( detalles == '' ) {
        console.log("5. No se añadirán detalles");
        send++;
    } else {
        // Comprobar longitud 
        if ( detalles.length <= 250 ) {
            console.log("5. La longitud de los detalles es correcta");
            send++;
        } else console.log("5. La longitud de los detalles es superior a 250");
    }
    let unidades = document.getElementById('unidades').value;
    unidades = parseFloat(unidades);
    if ( unidades > 0) {
        console.log("6. Cantidad de unidades correcta");
        send++;
    } else console.log("6. Las unidades no pueden ser 0 o menor");

    let imagen = document.getElementById('imagen').value;
    if ( imagen == '' ) {
        console.log("7. No se definió una ruta, se asignará img/default.png por defecto");
        imagen = 'img/default.png';
        send++;
    } else {
        console.log("7. Se asigno la ruta: ", imagen);
        send++;
    }
    return send == 7 ? true : false; // Todo debe de estar correcto para enviarse
}
