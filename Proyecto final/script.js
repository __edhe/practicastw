document.addEventListener('DOMContentLoaded', function(){
    let images = document.getElementsByClassName('principal-div-second');
    let presentation = document.getElementsByClassName('presentation');
    images[0].animate([
        {opacity: '.1'},
        {opacity: '1'}
    ],{
        duration: 600,
        iterations: 1
    });
    presentation[0].animate([
        {transform: 'scale(.8)'},
        {transform: 'scale(1)'}
    ],{
        duration: 500,
        iterations: 1
    });
    
    var animated = false
    window.addEventListener('scroll', function(evt) {
        var prices = document.getElementsByClassName('price');
        scroll = window.scrollY;
        
        if ( scroll >= 500 && animated == false) {
            prices[1].animate([
                {transform: 'scale(.98)'},
                {transform: 'scale(1)'},
                {transform: 'scale(.98)'}
            ],{
                duration: 1000,
                iterations: Infinity
            });
            animated = true;
        }
    });
    var nav = document.getElementsByClassName('navigator-button');
    for (let index = 0; index < nav.length; index++) {
        const element = nav[index];
        element.addEventListener('click', function() {
            ancla(index);
        });
    }
});

// Funciones:
