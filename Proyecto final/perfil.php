<?php 
    session_start();
?>

<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN'	'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'>
    <head>
        <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
        <title><?=$_SESSION['nombre'] ?></title>
        <link rel="icon" type="image/ico" href="img/favicon.png">
        <script src="https://kit.fontawesome.com/4c6ec24f19.js" crossorigin="anonymous"></script>
        <!-- <link rel="stylesheet" href="style-ingresar.css"> -->
        <link rel='stylesheet' href='style.css' type='text/css' media='screen' charset='utf-8'/>
        <link rel='stylesheet' href='style-perfil.css' type='text/css' media='screen' charset='utf-8'/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="script3.js"></script>
    </head>
    <body>
        <div class="bg-cover"></div>
        <div class="container">
            <div class="header_">
                <i class="fa fa-bars"></i>
                <i class="fa-solid fa-pen-to-square"></i>
            </div>
            <div class="middle">
                <img src='' alt="" class="user-pic" />
                <h4 class="name"><?php echo $_SESSION['nombre'],' ',$_SESSION['apellidos']?></h4>
                <h4 class="work"><?php echo $_SESSION['user'], ' | ',$_SESSION['tipo'], ' | ', $_SESSION['pais']?></h4>
                <h4 class="social"><i class="fa fa-facebook"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <i class="fa fa-twitter"></i></h4>
            </div>
            <div class="footer">
                <button class="btn-follow clic">Ir al catálogo</button><br/>
                <i class="fa fa-lock"></i>
                <br/>
            </div>
        </div>
        <div id="perfiles" style='display: none'>
            <div><h1>Seleccionar un perfil para continuar</h1></div>
            <div id="perfil">
                <?php 
                    if (!empty($_SESSION['data'])){
                        // echo count($_SESSION['data']);
                        for ($i=0; $i < count($_SESSION['data']); $i++) { 
                            // echo $_SESSION['data'][$i]['id'];
                            echo '<img src=',$_SESSION['data'][$i]['rutaImagen'],' alt="" class="user-pic" />';
                            echo '<div class="perfil_">',$_SESSION['data'][$i]['usuario'],'</div>';
                        }
                    }
                    
                ?>
            </div>
        </div>

        <div id="edicion" style="display: none">
            <form action="" method="post" id="form">
                <input type="text" name="usuario" 
                id="usuario" placeholder="Usuario" required>
                <input type="text" name="idioma" id="idioma" placeholder="Idioma" required><br>
                <input type="text" name="edad" id="edad" placeholder="Edad" required><br>
                <input type="radio" value="img/default1.jpg" name="imagen" class="imagen"> <label for="imagen1" required>Imagen 1</label>
                <input type="radio" value="img/default2.jpg" name="imagen" class="imagen"><label for="imagen2" required>Imagen 2</label>
                <input type="radio" value="img/default3.jpg" name="imagen" class="imagen"><label for="imagen3" required>Imagen3</label><br>
                <input type="radio" value="editar" name="ednu" class="ednu"> <label for="ednu" required>EDITAR</label>
                <input type="radio" value="nuevo" name="ednu" class="ednu"><label for="nuevo" required>NUEVO</label>
                <input type="submit" value="Guardar">
                <input type="reset" value="Cancelar">
            </form>
        </div>
    </body>
</html>