<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN'	'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'>
    <head>
        <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
        <title>Formulario productos</title>
        <link rel='stylesheet' href='style.css' type='text/css' media='screen' charset='utf-8'/>
    </head>
    <body>
    <form action="set_producto_v2.php" method="post">
        <fieldset>
            <legend>Formulario productos</legend>
            <ul>
                <li>Nombre: <input type="text" name="nombre" required></li>
                <li>Marca: <input type="text" name="marca" required></li>
                <li>Modelo: <input type="text" name="modelo" required></li>
                <li>Precio: <input type="number" name="precio" step="0.01" required></li>
                <li>Detalles: <br><textarea name="detalles" rows="4" cols="60" placeholder="No más de 250 caracteres de longitud" required></textarea></li>
                <li>Unidades: <input type="number" name="unidades" required></li>
                <li>Imagen: <input type="text" name="imagen" required></li>
            </ul>
            <input type="submit" value="Enviar">
        </fieldset>
    </form>
    </body>
</html>