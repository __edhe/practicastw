document.addEventListener('DOMContentLoaded', function(){
    document.getElementsByClassName('fa-pen-to-square')[0].addEventListener('click', function() {
        document.getElementById('edicion').style.display = "block"; 
        document.getElementById('perfiles').style.display = "none";
    });
    let form = document.getElementById('form');
    form.addEventListener('submit',actualizar);
    form.addEventListener('reset',function(){
        document.getElementById('edicion').style.display = 'none';
    });

    document.getElementsByClassName('btn-follow')[0].addEventListener('click', function() {
        document.getElementById('perfiles').style.display = "block";
        document.getElementById('edicion').style.display = "none";  
        $.ajax({
            type: 'post',
            url: 'getPerfiles.php',
            async: true,
        });
    });
    let perfil_ = document.getElementsByClassName('perfil_');
    for (let index = 0; index < perfil_.length; index++) {
        const element = perfil_[index];
        let perfil_n = index;
        element.addEventListener('click',function() {
            console.log("Seleccionado el perfil: ",perfil_n);
            $.ajax({
                type:'post',
                url: 'select_profile.php',
                async: true,
                data: {
                    perfil: perfil_n
                },
                success: function(result) {
                    console.log(result);
                    if(result){
                        console.log('Redirección');
                        let targetURL = 'catalogo.php';
                        let newURL = document.createElement('a');
                        newURL.href = targetURL;
                        document.body.appendChild(newURL);
                        newURL.click();
                    }
                }
            });
        });
    }
}); 
async function actualizar(ev){
    ev.preventDefault();
    const usuario = document.getElementById('usuario').value;
    const idioma = document.getElementById('idioma').value;
    const edad = document.getElementById('edad').value;
    var ednu = document.getElementsByClassName('ednu');
    var imagen = document.getElementsByClassName('imagen');

    for (let index = 0; index < imagen.length; index++) {
        const element = imagen[index];
        if (element.checked){
            // alert(element.value);
            imagen = element.value;
        }
    }
    for (let index = 0; index < ednu.length; index++) {
        const element = ednu[index];
        if (element.checked){
            // alert(element.value);
            ednu = element.value;
        }
    }


    $.ajax({
        type: 'post',
        url: 'update.php',
        async: true,
        data:{
            usuario: usuario, idioma: idioma, imagen: imagen, edad: edad, ednu: ednu
        },
        success: function(result){
            if (result == 'true'){
                console.log("Redireccion");
                let targetURL = 'perfil.php';
                let newURL = document.createElement('a');
                newURL.href = targetURL;
                document.body.appendChild(newURL);
                newURL.click();
            }
        }
    });
    

}