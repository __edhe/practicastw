<?php
namespace Webtechnologies\API;

use Webtechnologies\API;
require_once __DIR__ . '/DataBase.php';

class Productos extends DataBase{
    private $response;
    
    public function __construct($database='marketzone') {

        $this->response = array();
        parent::__construct($database);
    }

    public function getResponse() {
        // SE HACE LA CONVERSIÓN DE ARRAY A JSON
        return json_encode($this->response, JSON_PRETTY_PRINT);
    }
}

//$productos = new Productos();
?>