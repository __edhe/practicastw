<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
"http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="es">
	<?php
    $data = array();
	$eliminado = 1;
	if(isset($_GET['eliminado']))
		$eliminado = $_GET['eliminado'];
		
	// echo $eliminado;
	/** SE CREA EL OBJETO DE CONEXION */
	@$link = new mysqli('localhost', 'root', 'P@$$w0rd', 'marketzone');	

	/** comprobar la conexión */
	if ($link->connect_errno) 
	{
		die('Falló la conexión: '.$link->connect_error.'<br/>');
			/** NOTA: con @ se suprime el Warning para gestionar el error por medio de código */
	}
	
	/** Crear una tabla que no devuelve un conjunto de resultados */
	if ( $result = $link->query("SELECT * FROM productos WHERE eliminado = '$eliminado'") ) 
	{
		
		$row = $result->fetch_all(MYSQLI_ASSOC);
		/** útil para liberar memoria asociada a un resultado con demasiada información */
		foreach($row as $num => $registro) {            // Se recorren tuplas
			foreach($registro as $key => $value) {      // Se recorren campos
				$data[$num][$key] = $value;
			}
		}
		$result->free();
	}
	// echo json_encode($data, JSON_PRETTY_PRINT);

	$link->close();
	
	?>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<title>Producto</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
		<script>
            function modificar() {
                // se obtiene el id de la fila donde está el botón presinado
                var rowId = event.target.parentNode.parentNode.id;

                // se obtienen los datos de la fila en forma de arreglo
                var data = document.getElementById(rowId).querySelectorAll(".row-data");

                var nombre = data[0].innerHTML;
                var marca = data[1].innerHTML;
				var modelo = data[2].innerHTML;
				var precio = data[3].innerHTML;
				var unidades = data[4].innerHTML;
				var detalles = data[5].innerHTML;
                console.log(nombre,'\n',marca,'\n',modelo,'\n',precio,'\n',unidades,'\n');
				send2form(nombre, marca, modelo, precio, unidades, detalles);
            }

        </script>
	</head>
	<body>
		<h3>PRODUCTO</h3>

		<br/>
		
		<?php if( isset($row) ) { 
		
		?>
            <?php foreach($row as $num => $registro) {?>
			<table class="table">
				<thead class="thead-dark">
					<tr>
					<th scope="col">#</th>
					<th scope="col">Nombre</th>
					<th scope="col">Marca</th>
					<th scope="col">Modelo</th>
					<th scope="col">Precio</th>
					<th scope="col">Unidades</th>
					<th scope="col">Detalles</th>
					<th scope="col">Imagen</th>
					<th scope="col">Eliminado</th>
					<th scope="col">Modificar</th>
					</tr>
				</thead>
				<tbody>
					<tr id="<?= $data[$num]['id']; ?>">
						<th scope="row"><?= $data[$num]['id'] ?></th>
							<td class="row-data"><?= $data[$num]['nombre'] ?></td>
							<td class="row-data"><?= $data[$num]['marca'] ?></td>
							<td class="row-data"><?= $data[$num]['modelo']  ?></td>
							<td class="row-data"><?= $data[$num]['precio'] ?></td>
							<td class="row-data"><?= $data[$num]['unidades'] ?></td>
							<td class="row-data"><?= $data[$num]['detalles'] ?></td>
							<td class="row-data"><img style="width: 80px;"src=<?= $data[$num]['imagen'] ?> ></td>
							<td class="row-data"><?= $data[$num]['eliminado']?></td>
							<td class="row-data"><input type="button" 
								value="Modificar" 
								onclick="modificar()" /></td>
						</tr>
				</tbody>
			</table>

		<?php }}?>
		<script>
            function send2form(nombre, marca, modelo, precio, unidades, detalles) {
                var form = document.createElement("form");

                var nombreIn = document.createElement("input");
                nombreIn.type = 'text';
                nombreIn.name = 'nombre';
                nombreIn.value = nombre;
                form.appendChild(nombreIn);

                var marcaIn = document.createElement("input");
                marcaIn.type = 'text';
                marcaIn.name = 'marca';
                marcaIn.value = marca;
                form.appendChild(marcaIn);

				var modeloIn = document.createElement("input");
				modeloIn.type = 'text';
                modeloIn.name = 'modelo';
                modeloIn.value = modelo;
                form.appendChild(modeloIn);

				var precioIn = document.createElement("input");
				precioIn.type = 'number';
                precioIn.name = 'precio';
                precioIn.value = precio;
                form.appendChild(precioIn);

				var unidadesIn = document.createElement("input");
				unidadesIn.type = 'number';
                unidadesIn.name = 'unidades';
                unidadesIn.value = unidades;
                form.appendChild(unidadesIn);

				var detallesIn = document.createElement("input");
				detallesIn.type = 'text';
				detallesIn.name = 'detalles';
				detallesIn.value = detalles;

                console.log(form);

                form.method = 'POST';
                form.action = 'formulario_productos_v3.php';  

                document.body.appendChild(form);
                form.submit();
            }
        </script>
	</body>
</html>