// JSON BASE A MOSTRAR EN FORMULARIO
var baseJSON = {
    "precio": 0.0,
    "unidades": 1,
    "modelo": "XX-000",
    "marca": "NA",
    "detalles": "NA",
    "imagen": "img/default.png"
};

// FUNCIÓN CALLBACK DE BOTÓN "Buscar"
function buscarID(e) {
    /**
     * Revisar la siguiente información para entender porqué usar event.preventDefault();
     * http://qbit.com.mx/blog/2013/01/07/la-diferencia-entre-return-false-preventdefault-y-stoppropagation-en-jquery/#:~:text=PreventDefault()%20se%20utiliza%20para,escuche%20a%20trav%C3%A9s%20del%20DOM
     * https://www.geeksforgeeks.org/when-to-use-preventdefault-vs-return-false-in-javascript/
     */
    e.preventDefault();

    // SE OBTIENE EL ID A BUSCAR
    var id = document.getElementById('search').value;
    var template = '';

    // SE CREA EL OBJETO DE CONEXIÓN ASÍNCRONA AL SERVIDOR
    var client = getXMLHttpRequest();
    client.open('POST', './backend/read.php', true);
    client.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    client.onreadystatechange = function () {
        // SE VERIFICA SI LA RESPUESTA ESTÁ LISTA Y FUE SATISFACTORIA
        if (client.readyState == 4 && client.status == 200) {
            console.log('[CLIENTE]\n'+client.responseText);

            // SE OBTIENE EL OBJETO DE DATOS A PARTIR DE UN STRING JSON
            let productos = JSON.parse(client.responseText);    // similar a eval('('+client.responseText+')');
            console.log(productos);
            
            // SE VERIFICA SI EL OBJETO JSON TIENE DATOS
            if(Object.keys(productos).length > 0) {
                // SE CREA UNA LISTA HTML CON LA DESCRIPCIÓN DEL PRODUCTO
                for (let i in productos) {
                    let descripcion = '';
                    descripcion += '<li>precio: '+productos[i].precio+'</li>';
                    descripcion += '<li>unidades: '+productos[i].unidades+'</li>';
                    descripcion += '<li>modelo: '+productos[i].modelo+'</li>';
                    descripcion += '<li>marca: '+productos[i].marca+'</li>';
                    descripcion += '<li>detalles: '+productos[i].detalles+'</li>';
                
                    // SE CREA UNA PLANTILLA PARA CREAR LA(S) FILA(S) A INSERTAR EN EL DOCUMENTO HTML
                    template += `
                        <tr>
                            <td>${productos[i].id}</td>
                            <td>${productos[i].nombre}</td>
                            <td><ul>${descripcion}</ul></td>
                        </tr>
                    `;

                }
                 // SE INSERTA LA PLANTILLA EN EL ELEMENTO CON ID "productos"
                document.getElementById("productos").innerHTML = template;
                
            }
        }
    };
    client.send("id="+id);
}

// FUNCIÓN CALLBACK DE BOTÓN "Agregar Producto"
function agregarProducto(e) {
    e.preventDefault();

    var data = [];
    // SE OBTIENE DESDE EL FORMULARIO EL JSON A ENVIAR
    var productoJsonString = document.getElementById('description').value;
    // SE CONVIERTE EL JSON DE STRING A OBJETO
    var finalJSON = JSON.parse(productoJsonString);
    // SE AGREGA AL JSON EL NOMBRE DEL PRODUCTO
    finalJSON['nombre'] = document.getElementById('name').value;
    // SE OBTIENE EL STRING DEL JSON FINAL
    productoJsonString = JSON.stringify(finalJSON,null,2);

    // SE CREA EL OBJETO DE CONEXIÓN ASÍNCRONA AL SERVIDOR
    var client = getXMLHttpRequest();
    client.open('POST', './backend/create.php', true);
    client.setRequestHeader('Content-Type', "application/json;charset=UTF-8");
    client.onreadystatechange = function () {
        // SE VERIFICA SI LA RESPUESTA ESTÁ LISTA Y FUE SATISFACTORIA
        if (client.readyState == 4 && client.status == 200) {
            console.log(productoJsonString);
            
            JSON.parse(productoJsonString, (key, value) => {
                data[key] = value;
            });

            // console.log(data);

            // Validación Js
            if ( validacion(data) ) {
                // window.alert("Se enviaron los datos");
                console.log('[CLIENTE]\n'+client.responseText);
            }
            else 
                window.alert("Los datos son incorrectos");
        }
    };
    client.send(productoJsonString);
}

// SE CREA EL OBJETO DE CONEXIÓN COMPATIBLE CON EL NAVEGADOR
function getXMLHttpRequest() {
    var objetoAjax;

    try{
        objetoAjax = new XMLHttpRequest();
    }catch(err1){
        /**
         * NOTA: Las siguientes formas de crear el objeto ya son obsoletas
         *       pero se comparten por motivos historico-académicos.
         */
        try{
            // IE7 y IE8
            objetoAjax = new ActiveXObject("Msxml2.XMLHTTP");
        }catch(err2){
            try{
                // IE5 y IE6
                objetoAjax = new ActiveXObject("Microsoft.XMLHTTP");
            }catch(err3){
                objetoAjax = false;
            }
        }
    }
    return objetoAjax;
}

function init() {
    /**
     * Convierte el JSON a string para poder mostrarlo
     * ver: https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/JSON
     */
    var JsonString = JSON.stringify(baseJSON,null,2);
    document.getElementById("description").value = JsonString;
}


function validacion(data) {

    console.log("En función: ", data);

    var send = 0;
    
    if ( data['nombre'].length <= 100 ) { // nombre
        console.log("1. La longitud del nombre es correcta: (", data['nombre'].length,")");
        send++;
    } else {
        console.log("1. La longitud del nombre exede el permitido (", data['nombre'].length,") máximo: 100 carácteres");
    }

    let marcas = ['Xiaomi', 'Apple', 'Samsung', 'Motorola', 'Huawei'];

    let f = false;
    let c = 0;

    while (c < marcas.length && !f) {
        if (data['marca'] == marcas[c]) {
            f = true;
        } else f = false; c++;
    }
    if ( f ) {
        console.log("2. Se seleccionó una marca de la lista: ", data['marca']);
        send++;
    } else console.log("2. No se seleccionó una marca de la lista");
    
    if ( data['modelo'].length <= 25 ) {
        console.log("3. Longitud del modelo correcta: ", data['modelo'].length);
        // Comprobar alfanumérico
        send++;
    } else {
        console.log("3. La longitud del modelo es mayor a la  permitida", data['modelo'].length);
    }

    data['precio'] = parseFloat(data['precio']);
    if ( data['precio'] <= 99.99) {
        console.log("4. El precio es menor a 99.99")
    } else {
        console.log("4. El precio es correcto, mayor a 99.99");
        send++;
    }
    if ( data['detalles'] == '' ) {
        console.log("5. No se añadirán detalles");
        send++;
    } else {
        // Comprobar longitud 
        if ( data['detalles'].length <= 250 ) {
            console.log("5. La longitud de los detalles es correcta");
            send++;
        } else console.log("5. La longitud de los detalles es superior a 250");
    }
    
    data['unidades'] = parseFloat(data['unidades']);
    if ( data['unidades'] > 0) {
        console.log("6. Cantidad de unidades correcta");
        send++;
    } else console.log("6. Las unidades no pueden ser 0 o menor");

    if ( data['imagen'] == '' ) {
        console.log("7. No se definió una ruta, se asignará img/default.png por defecto");
        data['imagen'] = 'img/default.png';
        send++;
    } else {
        console.log("7. Se asigno la ruta: ", data['imagen']);
        send++;
    }
    return send == 7 ? true : false; // Todo debe de estar correcto para enviarse
}