$(document).ready(function(){

    listar();

    function listar() {
        $.ajax({
            url:'./list.php',
            type: 'GET',
            success: function(response){
                var series = JSON.parse(response);


                if(Object.keys(series).length > 0) {
                    let template = '';

                    for (let index = 0; index < series.length; index++) {
                        const element = series[index];
                        template += `
                            <div class="element-item web cssjava uxui"> 
                                <p class="logo"><img src="${element[index]['rutaPortada']}" alt=""></p>
                                <h3 class="name">${element[index]['titulo']}</h3>
                                <p class="subtitle">Temporadas: ${element[index]['numTemporadas']} Capítulos: ${element[index]['totalCapitulos']}</p>
                            </div>
                        `;
                    }                
                    $('.grid').html(template);
                }
            }
        });
        $.ajax({
            url:'./list_p.php',
            type: 'GET',
            success: function(response){
                var peliculas = JSON.parse(response);

                if(Object.keys(peliculas).length > 0) {
                    let template = '';

                    for (let index = 0; index < peliculas.length; index++) {
                        const element = peliculas[index];
                        template += `
                            <div class="element-item web cssjava uxui"> 
                                <p class="logo"><img src="${element[index]['rutaPortada']}" alt=""></p>
                                <h3 class="name">${element[index]['titulo']}</h3>
                                <p class="subtitle">Duracion: ${element[index]['duracion']}</p>
                            </div>
                        `;
                    }
                    $('.grid').append(template);
                }
            }
        });
    }
});
