<?php 
    require_once __DIR__ . '/Webtechnologies/models/User.php';
    require_once __DIR__ . '/Webtechnologies/views/UserTemplate.php';
    require_once __DIR__ . '/Webtechnologies/controllers/UserController.php';
    require_once __DIR__ . '/Webtechnologies/models/Account.php';
    require_once __DIR__ . '/Webtechnologies/views/AccountTemplate.php';
    require_once __DIR__ . '/Webtechnologies/controllers/AccountController.php';

?>