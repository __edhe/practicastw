<?php 
    function first() {
        $n = $_GET['numero'];
        if ( ($n % 7 == 0) && ($n % 5 == 0) ) echo "$n es múltiplo de 5 y 7";
        else echo "$n no es múltiplo de 5 y 7 <br>";
    }
    function second() {
        $numbers = array (
            array(),
            array(),
            array()
        );
        
        $f = false;
        $count = 0;
        while ( $f != true ) {
            $count++;
            $numbers[$count][0] = rand(1, 999); 
            $numbers[$count][1] = rand(1, 999); 
            $numbers[$count][2] = rand(1, 999); 

            echo $numbers[$count][0] . "&emsp;" . $numbers[$count][1] . "&emsp;" . $numbers[$count][0] . "<br>";
            
            ( ($numbers[$count][0] % 2 != 0) && ($numbers[$count][1] % 2 == 0) && ($numbers[$count][2] % 2 != 0) ) ? $f = true :$f = false ;
        }

        echo $count*3 . " números obtenidos en " . $count . " iteraciones";
    }
    // first();
    function third() {
        $f = false;
        $n2 = $_GET['numero2'];
        while ( $f != true) {
            $n_random = rand(1,999);
            if ( $n_random % $n2 == 0 ) echo "<br><br>$n_random es múltiplo de $n2";
            else echo "<br><br>$n_random no es múltiplo de $n2 <br>";
            $f = true;
        }   
        do {
            $n_random = rand(1,999);
            if ( $n_random % $n2 == 0 ) {
                echo "<br>$n_random es múltiplo de $n2";
                $f == true;
            }
            else echo "<br>$n_random no es múltiplo de $n2 <br>";
        } while (!$f);
    }
    // third();
    function fourth() {
        $letras = array();
        for ( $i = 97; $i < 123; $i++ ) {
            $letras[$i] = chr($i);
        }
        foreach ($letras as $key => $value) {
            echo $key . "&emsp;" . $value . "<br>";
        }
    }
    function fifth() {
        $edad = $_POST['edad'];
        $sexo = $_POST['sexo'];

        // echo $edad." ".$sexo;
        if ( ($edad >= 18 && $edad <= 35) && $sexo == "femenino" ) 
            echo "Bienvenida, usted está en el rango de edad permitido";
        else echo "Usted necesita tener de 18 a 35 años y ser mujer";

    }
    // fifth();
    function sixth() {
        $vehiculo = array(
            "UBN6338" => array(
                "Auto" => array(
                    "Marca" => "HONDA",
                    "Modelo" => "2020",
                    "Tipo" => "CAMIONETA"
                ),
                "Propietario" => array(
                    "Nombre" => "Alfonso Esparza",
                    "Ciudad" => "Puebla, Pue.",
                    "Direccion" => "C.U., Jardines de San Manuel"
                )
                ),
            "UBN6339" => array(
                "Auto" => array(
                    "Marca" => "MAZDA",
                    "Modelo" => "2019",
                    "Tipo" => "SEDAN"
                ),
                "Propietario" => array(
                    "Nombre" => "Ma. del Consuelo Molina",
                    "Ciudad" => "Puebla, Pue.",
                    "Direccion" => "97 oriente"
                )
                ),
            "UBN6340" => array(
                "Auto" => array(
                    "Marca" => "DODGE",
                    "Modelo" => "2011",
                    "Tipo" => "CAMIONETA"
                ),
                "Propietario" => array(
                    "Nombre" => "Luis Hernandez",
                    "Ciudad" => "Huamantla, Tlaxcala.",
                    "Direccion" => "Prol Hidalgo"
                )
            ),
            "UBN6341" => array(
                "Auto" => array(
                    "Marca" => "FORD",
                    "Modelo" => "2018",
                    "Tipo" => "SEDAN"
                ),
                "Propietario" => array(
                    "Nombre" => "Alfredo Sanchez",
                    "Ciudad" => "Puebla, Pue.",
                    "Direccion" => "24 Sur"
                )
                ),
            "UBN6342" => array(
                "Auto" => array(
                    "Marca" => "SEAT",
                    "Modelo" => "2022",
                    "Tipo" => "SEDAN"
                ),
                "Propietario" => array(
                    "Nombre" => "Candelaria Sanchez",
                    "Ciudad" => "Tlaxcala, Tlax.",
                    "Direccion" => "Negrete Oriente"
                )
            ),
            "UBN6343" => array(
                "Auto" => array(
                    "Marca" => "KIA",
                    "Modelo" => "2019",
                    "Tipo" => "HACHBACK"
                ),
                "Propietario" => array(
                    "Nombre" => "Eduardo Hernandez",
                    "Ciudad" => "Puebla, Pue.",
                    "Direccion" => "Rio Grijalba"
                )
                ),
            "UBN6344" => array(
                "Auto" => array(
                    "Marca" => "CHEVROLET",
                    "Modelo" => "2021",
                    "Tipo" => "CAMIONETA"
                ),
                "Propietario" => array(
                    "Nombre" => "TESLA",
                    "Ciudad" => "Puebla, Pue.",
                    "Direccion" => "Insurgentes"
                )
            ),
            "UBN6345" => array(
                "Auto" => array(
                    "Marca" => "AUDI",
                    "Modelo" => "2008",
                    "Tipo" => "SEDAN"
                ),
                "Propietario" => array(
                    "Nombre" => "Justino Saldaña",
                    "Ciudad" => "Puebla, Pue.",
                    "Direccion" => "Rio Papagayo"
                )
                ),
            "UBN6346" => array(
                "Auto" => array(
                    "Marca" => "FORD",
                    "Modelo" => "2021",
                    "Tipo" => "CAMIONETA"
                ),
                "Propietario" => array(
                    "Nombre" => "Felix Perez",
                    "Ciudad" => "Puebla, Pue.",
                    "Direccion" => "9 Norte"
                )
            ),
            "UBN6347" => array(
                "Auto" => array(
                    "Marca" => "MAZDA",
                    "Modelo" => "2019",
                    "Tipo" => "HATCHBACK"
                ),
                "Propietario" => array(
                    "Nombre" => "Yoselin Velazques",
                    "Ciudad" => "Puebla, Pue.",
                    "Direccion" => "Av 6 Oeste"
                )
                ),
            "UBN6348" => array(
                "Auto" => array(
                    "Marca" => "FORD",
                    "Modelo" => "2017",
                    "Tipo" => "SEDAN"
                ),
                "Propietario" => array(
                    "Nombre" => "Gabriel Carbajal",
                    "Ciudad" => "Puebla, Pue.",
                    "Direccion" => "16 de Septiembre"
                )
            ),
            "UBN6349" => array(
                "Auto" => array(
                    "Marca" => "MAZDA",
                    "Modelo" => "2017",
                    "Tipo" => "CAMIONETA"
                ),
                "Propietario" => array(
                    "Nombre" => "Paula Cordero",
                    "Ciudad" => "Puebla, Pue.",
                    "Direccion" => "3 Norte"
                )
                ),
            "UBN6350" => array(
                "Auto" => array(
                    "Marca" => "HONDA",
                    "Modelo" => "2022",
                    "Tipo" => "CAMIONETA"
                ),
                "Propietario" => array(
                    "Nombre" => "Sinai Flores",
                    "Ciudad" => "Puebla, Pue.",
                    "Direccion" => "2 Oriente"
                )
            ),
            "UBN6351" => array(
                "Auto" => array(
                    "Marca" => "DODGE",
                    "Modelo" => "2018",
                    "Tipo" => "SEDAN"
                ),
                "Propietario" => array(
                    "Nombre" => "Ivonne Venegas",
                    "Ciudad" => "Puebla, Pue.",
                    "Direccion" => "5 Sur"
                )
                ),
            "UBN6352" => array(
                "Auto" => array(
                    "Marca" => "Tesla",
                    "Modelo" => "2020",
                    "Tipo" => "SEDAN"
                ),
                "Propietario" => array(
                    "Nombre" => "Samantha Vega",
                    "Ciudad" => "Puebla, Pue.",
                    "Direccion" => "10 Norte"
                )
            )
        );
        $matricula = $_POST['matricula'];
        echo $matricula . "<br><br><br>";
        if ($matricula != '') {
            var_dump($vehiculo[$matricula]);
        } else {
            echo "No se estableción matricula <br><br>";
            var_dump($vehiculo);
        }
    }
    sixth();
?>