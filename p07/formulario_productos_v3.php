<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN'	'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'>
    <head>
        <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
        <title>Formulario productos</title>
        <link rel='stylesheet' href='style.css' type='text/css' media='screen' charset='utf-8'/>
        
    </head>
    <body>
    <form action="set_datos.php" method="post" id="form" name="form">
        <fieldset>
            <legend>Formulario productos</legend>
            <ul>
                <li>Nombre: <input type="text" name="nombre" required id="nombre"  value="<?= !empty($_POST['nombre'])?$_POST['nombre']:$_GET['nombre'] ?>"></li>
                <li>Marca:
                    <select name="marca" id="marca" value="<?= !empty($_POST['marca'])?$_POST['marca']:$_GET['marca'] ?>">
                        <option value="Xiaomi">Xiaomi</option>
                        <option value="Apple">Apple</option>
                        <option value="Samsung">Samsung</option>
                        <option value="Motorola">Motorola</option>
                        <option value="Huawei">Huawei</option>
                    </select>
                </li>
                <li>Modelo: <input type="text" name="modelo" required id="modelo" value="<?= !empty($_POST['modelo'])?$_POST['modelo']:$_GET['modelo'] ?>"></li>
                <li>Precio: <input type="number" name="precio" id="precio" step="0.01" required  value="<?= !empty($_POST['precio'])?$_POST['precio']:$_GET['precio'] ?>"></li>
                <li>Detalles: <br><textarea name="detalles" id="detalles" rows="4" cols="60" placeholder="No más de 250 caracteres de longitud"></textarea></li>
                <li>Unidades: <input type="number" name="unidades" id="unidades" required  value="<?= !empty($_POST['unidades'])?$_POST['unidades']:$_GET['unidades'] ?>"></li>
                <li>Imagen: <input type="text" id="imagen" name="imagen"></li>
                <input type="text" hidden value="<?= !empty($_POST['modelo'])?$_POST['modelo']:$_GET['modelo'] ?>" name="mod_anterior">
            </ul>
            <input type="submit"  value="Enviar" id="submit">
        </fieldset>
    </form>
    </body>
</html>