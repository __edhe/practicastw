<?php
    include_once __DIR__.'/database.php';
    session_start();
    $id = $_SESSION['id'];
    $id = intval($id);
    $_SESSION['data'] = array();

    if ( $result = $conexion->query("SELECT * FROM perfil WHERE idCuenta = $id") ) {
        $rows = $result->fetch_all(MYSQLI_ASSOC);
        if(!is_null($rows)) {
            foreach($rows as $num => $row) {
                foreach($row as $key => $value) {
                    $data[$num][$key] = $value;
                    $_SESSION['data'][$num][$key] = $value;
                }
            }
        }
        $result->free();
    } else {
        die('Query Error: '.mysqli_error($conexion));
    }
    $conexion->close();
    echo json_encode($data, JSON_PRETTY_PRINT);
?>