<?php 
    session_start();
    include_once __DIR__.'/database.php';
    $id_perfil = intval($_SESSION['id_perfil']);

    // echo('id de cuenta: '.$_SESSION['id']);
    // echo(' | idPerfil: '.$_SESSION['id_perfil']);
    
    $perfil = $_SESSION['data'][$id_perfil];

    $_SESSION['edad'] = $perfil['edad'];
    // Consulta para el perfil:

    // var_dump($perfil);
    
?>

<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN'	'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'>
    <head>
        <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
        <title>Catálogo | <?= $perfil['usuario']; ?></title>
        <link rel='stylesheet' href='style-catalogo.css' type='text/css' media='screen' charset='utf-8'/>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="script4.js"></script>
    </head>
    <body>
        
        <div id="container-profile_img"><img id="profile_img" src="<?= $perfil['rutaImagen'];?>" alt=""></div>
        <h2 class="h2">Catálogo para <?= $perfil['usuario']; ?> </h2>
        <h1 class="h1">Películas y series</h1>
        <h4 class="h4">Mostrando para <?=$_SESSION['edad']; ?> años</h4>

        <div class="filters">
                <form class="form-inline my-2 my-lg-0">
                    <input class="form-control mr-sm-2" name="search" id="search" type="search" placeholder="ID, marca o descripción" aria-label="Search">
                    <button class="btn btn-success my-2 my-sm-0" type="submit">Buscar</button>
                </form>
        </div>

        <div class="grid">
            
        </div>
    </body>
</html>