<?php 
    session_start();
    if(!empty($_SESSION['id'])){ // Si existe una sesión se redirecciona
        header("Location: perfil.php");
    };
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" type="image/ico" href="favicon.ico">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="style-ingresar.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link rel="icon" type="image/ico" href="img/favicon.png">
    <script src="script2.js"></script>
    <script src="funciones.js"></script>
    <title>Iniciar</title>
</head>
<body>

    <div id="cont-txt-form">
        <div id="barra">
            <div class="barra-div" id="login">
                <h1>¿No estás registrado?</h1><br>
                <p>Registrate para acceder a tu cuenta y gozar de las miles* de series y películas que tenemos para tí.</p>
                <span id="btn-login">Sign up</span>
            </div>
            <div class="barra-div" id="singup">
                <h1>¿Ya tienes una cuenta?</h1><br>
                <p>Inicia sesión ahora y comienza a disfrutar tus series y películas favoritas.</p>
                <span id="btn-singup">Log in</span>
                
            </div>
        </div>
    </div>
    <div id="cont-form">
        <p id="logo">C<strong>M</strong></p> 
        <form method="post" id="singup-form" action="">
            <input type="text" placeholder="Nombre" required id="nombre">
            <input type="text" placeholder="Usuario" required id="usuario">
            <input type="text" placeholder="Apellido" required id="apellido">
            <input type="text" placeholder="Tipo (free, familiar, premium)" required id="tipo">
            <input type="text" placeholder="Pais" required id="pais">
            <input type="text" name="tarjeta" id="tarjeta" placeholder="tarjeta">
            <input type="email" name="email" id="email" placeholder="Correo electrónico" required>
            <label id="msg" for="password" style="display: none; color: rgb(243, 243, 243);">Así se ve</label>
            <input type="password" name="password" id="password" placeholder="Contraseña - (8 caracteres mínimo) " required>
            <input type="password" name="re-password" id="re-password" placeholder="Confirmar contraseña" required>
            <p>Al registrarse está aceptando nuestros términos y condiciones de servicio.</p>     
            <input type="submit" value="Guardar">
            <input type="reset" value="Cancelar">
        </form>

        <form method="post" id="login-form" style="display: none;">
            <input type="email" name="email_" id="email_" placeholder="Correo electrónico" required>
            <label id="msg" for="password" style="display: none; color: rgb(243, 243, 243);">Así se ve</label>
            <input type="password" name="password_" id="password_" placeholder="Contraseña - (8 caracteres mínimo) " required>

            <p id="msg_r">Es un gusto tenerte de regreso</p>     
            <input type="submit" value="Acceder">
        </form>
    </div>
    <a href="index.html" id="regresar">Regresar a inicio</a>
</body>
</html>