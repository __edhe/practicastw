function validaciones(e){
    switch(e){
        case 'name': 
            let nombre = document.getElementById('name').value;
            console.log(nombre.length);
            if ( nombre.length > 0 && nombre.length <=100 ) { // nombre
                $('#msg2').html("1. La longitud del nombre es correcta: ("+ nombre.length +")");
            } else if (nombre.length == 0){
                $('#msg2').html('El campo nombre es requerido')
            } else{
                $('#msg2').html("1. La longitud del nombre exede el permitido ("+ nombre.length +") máximo: 100 carácteres");
            }
            break;
        case 'precio':
            let precio = document.getElementById('precio').value;
            precio = parseFloat(precio);
            if ( precio <= 99.99 && precio != 0) {
                $("<p>2. El precio es menor a 99.99</p>").appendTo("#msg2");
            } else {
                $("<p>2. El precio es correcto, mayor a 99.99</p>").appendTo("#msg2");
            }
            break;
        case 'unidades':
            let unidades = document.getElementById('unidades').value;
            unidades = parseFloat(unidades);
            if ( unidades > 0) {
                $("<p>3. Cantidad de unidades correcta</p>").appendTo("#msg2");
            } else $("<p>3. Las unidades no pueden ser 0 o menor</p>").appendTo("#msg2");
            break;
        case 'modelo':
            let modelo = document.getElementById('modelo').value;
            if ( modelo.length <= 25 ) {
                $("<p>4. Longitud del modelo correcta: "+ modelo.length+"</p>").appendTo("#msg2");
            } else {
                $("<p>4. La longitud del modelo es mayor a la  permitida"+ modelo.length+"</p>").appendTo("#msg2");
            }
            break;
        case 'marca':
            let marca = document.getElementById("marca").value;
            let marcas = ['Xiaomi', 'Apple', 'Samsung', 'Motorola', 'Huawei'];

            let f = false;
            let c = 0;

            while (c < marcas.length && !f) {
                if (marca == marcas[c]) {
                    f = true;
                } else f = false; c++;
            }
            if ( f ) {
                $("<p>5. Se seleccionó una marca de la lista: "+ marca +"</p>").appendTo("#msg2");
            } else $("<p>5. No se seleccionó una marca de la lista</p>").appendTo("#msg2");
            break;
        case 'detalles':
            let detalles = document.getElementById('detalles').value;
            if ( detalles == '' ) {
                $("6. No se añadirán detalles").appendTo("#msg2");
            } else {
                // Comprobar longitud 
                if ( detalles.length <= 250 ) {
                    $("<p>6. La longitud de los detalles es correcta</p>").appendTo('#msg2');
                } else $("<p>6. La longitud de los detalles es superior a 250</p>").appendTo("#msg2");
            }
            break;
        case 'imagen':
            let imagen = document.getElementById('imagen').value;
            if ( imagen == '' ) {
                $("<p>7. No se definió una ruta, se asignará img/default.png por defecto</p>").appendTo("#msg2");
                imagen = 'img/default.png';
            } else {
                $("<p>7. Se asigno la ruta: "+ imagen+"</p>").appendTo("#msg2");
            }
            break;
    }
}

