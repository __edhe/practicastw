// JSON BASE A MOSTRAR EN FORMULARIO
var baseJSON = {
    "precio": 0.0,
    "unidades": 1,
    "modelo": "XX-000",
    "marca": "NA",
    "detalles": "NA",
    "imagen": "img/default.png"
};

function init() {
    /**
     * Convierte el JSON a string para poder mostrarlo
     * ver: https://developer.mozilla.org/es/docs/Web/JavaScript/Reference/Global_Objects/JSON
     */
    var JsonString = JSON.stringify(baseJSON,null,2);
    document.getElementById("description").value = JsonString;

    listarProductos();
}

function listarProductos() {
    $.ajax({
        url: 'backend/product-list.php',
        type: 'GET',
        success: function(response) {
            const products = JSON.parse(response);
            let template = '';
            products.forEach(product => {
                template += `
                        <tr productId="${product.id}">
                        <td>${product.id}</td>
                        <td>
                        <a href="#" class="product-item">
                        ${product.nombre} 
                        </a>
                        </td>
                        <td>${product.detalles}</td>
                        <td>
                        <button class="product-delete btn btn-danger">
                        Delete 
                        </button>
                        </td>
                        </tr>
                    `
            });
            $('#products').html(template);
        }
    });
}

$(document).ready(function() {

    // Global Settings
    let edit = false;

    // Testing Jquery
    console.log('jquery is working!');
    listarProductos();
    $('#product-result').hide();

    // search key type event
    $('#search').keyup(function() {
        if($('#search').val()) {
            let search = $('#search').val();
            $.ajax({
                url: 'backend/product-search.php',
                data: {search},
                type: 'POST',
                success: function (response) {
                    let products = JSON.parse(response);
                    let template = '';
                    // console.log(copy);

                    // console.clear();
                    $("#products").empty();
                    products.forEach(product => {
                        template += `
                                <tr productId="${product.id}">
                                <td>${product.id}</td>
                                <td>
                                <a href="#" class="product-item">
                                ${product.nombre} 
                                </a>
                                </td>
                                <td>${product.detalles}</td>
                                <td>
                                <button class="product-delete btn btn-danger">
                                Delete 
                                </button>
                                </td>
                                </tr>
                            `
                    });

                    // console.log(template);
                    $('#product-result').show();
                    $('#products').html(template);                    
                } 
            })
        }
    });
    
    $('#product-form').submit(e => {
        e.preventDefault();
        var data = [];
        const detalles = $('#description').val();
        let finalJSON = JSON.parse(detalles);
        let finalJsonString = JSON.stringify(finalJSON, null, 2);
        const url = edit === false ? 'backend/product-add.php' : 'backend/product-edit.php';
        // console.log(finalJsonString, url);
        JSON.parse(finalJsonString, (key, value) => {
            data[key] = value;
        })
        if (edit == true) {
            if ( validacion(data) ) {
                $.post(url, {finalJsonString}, (response) => {
                    console.log(response);
                    $('#product-form').trigger('reset');
                    listarProductos();
                });
            } else 
                alert("Los datos son incorrectos");
        } else {
            finalJSON['nombre'] = $("#name").val();
            finalJsonString = JSON.stringify(finalJSON, null, 2);
            alert(finalJsonString);
            alert("No se validara");
            $.post(url, {finalJsonString}, (response) => {
                console.log(response);
                $('#product-form').trigger('reset');
                listarProductos();
            });
        }
    });

    // Get a Single product by Id 
    $(document).on('click', '.product-item', (e) => {
        const element = $(this)[0].activeElement.parentElement.parentElement;
        const id = $(element).attr('productId'); // Obtenemos el ID
        // console.log(element);
        console.log(id);
        var template = '';
        $.post('backend/product-single.php', {id}, (response) => {
            let productos = JSON.parse(response);
            console.log(productos);
            console.log(productos[0].nombre);
            $('#name').val(productos[0].nombre);

            var JsonString = JSON.stringify(productos,null,2);
            document.getElementById("description").value = JsonString;
            
            edit = true;
            $("#btn-submit").text("Modificar");
            
        });
        e.preventDefault();
    });


    // Delete a Single product
    $(document).on('click', '.product-delete', (e) => {
        if(confirm('Are you sure you want to delete it?')) {
            const element = $(this)[0].activeElement.parentElement.parentElement;
            const id = $(element).attr('productId');
            $.post('backend/product-delete.php', {id}, (response) => {
                listarProductos();
            });
        }
    });

});

function validacion(data) {

    console.log("En función: ", data);

    var send = 0;
    
    if ( data['nombre'].length <= 100 ) { // nombre
        console.log("1. La longitud del nombre es correcta: (", data['nombre'].length,")");
        send++;
    } else {
        console.log("1. La longitud del nombre exede el permitido (", data['nombre'].length,") máximo: 100 carácteres");
    }

    let marcas = ['Xiaomi', 'Apple', 'Samsung', 'Motorola', 'Huawei'];

    let f = false;
    let c = 0;

    while (c < marcas.length && !f) {
        if (data['marca'] == marcas[c]) {
            f = true;
        } else f = false; c++;
    }
    if ( f ) {
        console.log("2. Se seleccionó una marca de la lista: ", data['marca']);
        send++;
    } else console.log("2. No se seleccionó una marca de la lista");
    
    if ( data['modelo'].length <= 25 ) {
        console.log("3. Longitud del modelo correcta: ", data['modelo'].length);
        // Comprobar alfanumérico
        send++;
    } else {
        console.log("3. La longitud del modelo es mayor a la  permitida", data['modelo'].length);
    }

    data['precio'] = parseFloat(data['precio']);
    if ( data['precio'] <= 99.99) {
        console.log("4. El precio es menor a 99.99")
    } else {
        console.log("4. El precio es correcto, mayor a 99.99");
        send++;
    }
    if ( data['detalles'] == '' ) {
        console.log("5. No se añadirán detalles");
        send++;
    } else {
        // Comprobar longitud 
        if ( data['detalles'].length <= 250 ) {
            console.log("5. La longitud de los detalles es correcta");
            send++;
        } else console.log("5. La longitud de los detalles es superior a 250");
    }
    
    data['unidades'] = parseFloat(data['unidades']);
    if ( data['unidades'] > 0) {
        console.log("6. Cantidad de unidades correcta");
        send++;
    } else console.log("6. Las unidades no pueden ser 0 o menor");

    if ( data['imagen'] == '' ) {
        console.log("7. No se definió una ruta, se asignará img/default.png por defecto");
        data['imagen'] = 'img/default.png';
        send++;
    } else {
        console.log("7. Se asigno la ruta: ", data['imagen']);
        send++;
    }
    return send == 7 ? true : false; // Todo debe de estar correcto para enviarse
}