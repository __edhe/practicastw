<?php 
    // libxml_use_internal_errors(true);

    // $xml = new DOMDocument();
    // $xml->load('serviciovod4.xml');

    // if ( !$xml->schemaValidate('serviciovod.xsd') ) {
    //     print '<b>DOMDocument::schemaValidate() Generated Errors!</b>';
    //     libxml_display_errors();
    // }


    libxml_use_internal_errors(true);
    
    $xml = new DOMDocument();
    $doc = file_get_contents('serviciovod4.xml');
    
    $xml->loadXML($doc);
    
    $xsd = 'serviciovod.xsd';

    if ( !$xml->schemaValidate($xsd) ) {
        $errors = libxml_get_errors();
        $noError = 1;
        $lista = '';

        foreach ( $errors as $error ) {
            $lista = $lista.'['.($noError++).']: '.$error->message.'';
            echo $lista;
        }
    } 
?>