<?php 
    if(!empty($_SESSION['id'])){ // Si existe una sesión se redirecciona
        header("Location: perfil.php");
    };
?>

<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN'	'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'>
    <head>
        <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
        <title>Page Title</title>
        <link rel='stylesheet' href='style.css' type='text/css' media='screen' charset='utf-8'/>
        <link rel="icon" type="image/ico" href="img/favicon.png">
        <script src="https://kit.fontawesome.com/4c6ec24f19.js" crossorigin="anonymous"></script>
        <script src="script.js"></script>
        <script src="funciones.js"></script>
    </head>
    <body>
        <div class="my-info">Sitio desarrollado por Eduardo Hernández | Copyright 2022</div>
        <div class="header">
            <a href="#"><img src="img/favicon.png" alt=""></a>
            <div class="header-navigator">
                <button type="button" class="navigator-button">
                    <p>Nosotros</p>
                </button>
                <button type="button" class="navigator-button">
                    <p>Planes</p>
                </button>
                <button type="button" class="navigator-button">
                    <p>Catálogo</p>
                </button>
                <button type="button" class="navigator-button">
                    <p>Registrarse</p>
                </button>
            </div>
        </div>

        <div class="principal-wrapper">
            <!-- presentation and image representative -->
            <div class="principal-div-first">
                <h1 class="presentation">
                    Películas
                    <br>
                    y series
                    <br>
                    para todos
                </h1>
                <p>Servicio de streaming que ofrece una gran variedad de películas y series.</p>
                <button class="clic">¡Conoce nuestro catálogo!</button>
            </div>
            <div class="principal-div-second">
                <img class="left-slider secondary-slider" src="img/img1.jpg" alt="">
                <img src="img/Rick_and_Morty_svg.svg" alt="">
                <img class="slider-middle first-slider" src="img/img2.jpg" alt="">
                <div class="title-wrapp">
                    <span></span>
                    <img src="img/breaking_bad_svg.svg" alt="">
                    <span></span>
                </div>
                <img class="right-slider secondary-slider" src="img/img3.jpg" alt="">
                <img src="img/stranger_things_svg.svg" alt="">
                
            </div>    
        </div>
        

        <div class="divider"></div>
        <div id="comenzar">
            <span>¿Estás listo?</span>
            <h1>¡Comienza ahora! <br> Las mejores películas y séries esperan por tí.</h1>
            <p id="registro" onclick="location.href='ingresar.php'">Registrarse</p>
        </div>
        <div class="divider"></div>

        <div class="secondary-wrapper">
            <div class="title-price">
                <h1>¿Ya conoces nuestros planes?</h1><br>
                <p>Planes que se adaptan a cualquier tipo de cartera y para cualquier tipo de usuario</p>
            </div>
            <div class="prices-container">
                <div class="price">
                    <h1>BÁSICO <br>
                        FREE
                    </h1><br>
                    <ul>
                        <li>Series y películas disponibles</li>
                        <li>Hasta 1 perfiles</li>
                        <li>1 dispositivo simultaneo</li>
                        <li>2 h de visualización gratis</li>
                    </ul>
                </div>
                <div class="price">
                    <h1>FAMILIAR <br>
                        $250
                    </h1><br>
                    <ul>
                        <li>Series y películas disponibles</li>
                        <li>Hasta 7 perfiles</li>
                        <li>5 dispositivos simultaneos</li>
                        <li>Visualización ilimitada</li>
                        <li>Ultra HD disponible</li>
                    </ul>
                </div>
                <div class="price">
                    <h1>PREMIUM <br>
                        $110
                    </h1><br>
                    <ul>
                        <li>Series y películas disponibles</li>
                        <li>Hasta 7 perfiles</li>
                        <li>3 dispositivos simultaneos</li>
                        <li>HD disponible</li>
                    </ul>
                </div>
            </div>
        </div>
    </body>
    <div class="divider divider-l"></div>
    <footer>
        <section>
            <center><h1>Comentarios</h1></center><br>
            <p>La creación de esta página fue inspirada por el <a href="https://youtu.be/UokZMBmnUGM" target="_blank">video de Youtube de Nate Gentille</a>.</p> <br>
            <p>Surgió mientras estaba sentado frente al ordenador pensando en el diseño para crear la página, sin embargo, no tuve ninguna idea, entre a Youtube y el primer video recomendado fue el de Nate, entré y disfrute la música de ese video (aunque en lo personal, ese no es mi género preferido).</p><br>
            <p>Entonces decidí crear este sitio, ya que la música de aquel video me llevó a un estado de relajación y de inspiración.</p>
        </section>
        <section>
            <p id="logo">C<strong>M</strong></p> 
            <p>Visita mis redes sociales:</p>
            <a href="https://www.facebook.com/eduherr/" target="_blank"><i class="fab fa-facebook-square"></i></a>
            <a href="https://www.instagram.com/__edhe/" target="_blank"><i class="fab fa-instagram-square"></i></a><br><br>
            <a href="https://www.facebook.com/eduherr/" target="_blank">www.facebook.com/eduherr</a><br>
            <a href="https://www.instagram.com/__edhe/" target="_blank">www.instagram.com/__edhe</a>
            
            <p id="derechos">&#169; Todos los derechos reservados 2022.</p>
        </section>
        <section id="acerca">
            <h1>Acerca de mi.</h1><br>
            <p>José Eduardo Hernández Rodríguez.<br>Matricula: 201938227<br>
            Ingeniería en tecnologías de la información.</p>
            <img src="img/yo.png" alt="">
        </section>
    </footer>
</html>