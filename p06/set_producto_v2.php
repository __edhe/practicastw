<?php
$nombre = $_POST['nombre'];
$marca  = $_POST['marca'];
$modelo = $_POST['modelo'];
$precio = $_POST['precio'];
$detalles = $_POST['detalles'];
$unidades = $_POST['unidades'];
$imagen   = $_POST['imagen'];
$eliminado = 0;

// Limpieza de los datos:
$precio = floatval($precio); // Esto para poder insertarlo en la bd → requiere double 
$unidades = intval($unidades); // Para poder insertarlo se requiere entero

// Verificación de variables 
    
    $flag = true; // true: se guarda, false: se rechaza

    if ( strlen($nombre ) > 100 || !is_string($nombre) ) {echo "El nombre es demasiado largo o no es el formato correcto" . "<br>"; $flag = false;}
    if ( strlen($marca) > 25 || !is_string($marca) ) {echo "La marca es demasiado largo o no es el formato correcto" . "<br>"; $flag = false;}
    if ( strlen($modelo) > 25 || !is_string($modelo) ) {echo "El modelo es demasiado largo o no es el formato correcto" . "<br>"; $flag = false;}
    if ( !is_double($precio) || $precio <= 0 ) { echo "El precio no es de punto flotante o es muy grande o menor a cero" . "<br>"; $flag = false;}
    if ( strlen($detalles) > 250 ) {echo "La longitud de los detalles es más largo de lo aceptado" . "<br"; $flag = false;}
    if ( $unidades <=0 || strlen($unidades) > 11 || !is_int($unidades) ) {"Las unidades no es entera, es muy grande o menor a cero" . "<br>"; $flag = false;}
    if ( strlen($imagen) > 100 || !is_string($imagen) ) {echo "La imagen no tiene el formato correcto" . "<br>"; $flag = false;}

    if ( $flag ) {
        echo "Se subirá a la base de datos<br>";
        echo $nombre . " - ". gettype($nombre) . "<br>" . $marca . " - ". gettype($marca) . "<br>" . $modelo . " - ". gettype($modelo) . "<br>" . $precio . " - ". gettype($precio) ."<br>" . $detalles. " - ". gettype($detalles) ."<br>" . $unidades. " - ". gettype($unidades) ."<br>" . $imagen. " - ". gettype($imagen) ."<br>";

        @$link = new mysqli('localhost', 'root', 'P@$$w0rd', 'marketzone');	

        /** comprobar la conexión */
        if ($link->connect_errno) 
        {
            die('Falló la conexión: '.$link->connect_error.'<br/>');
            /** NOTA: con @ se suprime el Warning para gestionar el error por medio de código */
        }

        /** Crear una tabla que no devuelve un conjunto de resultados */
        $sql = "INSERT INTO productos VALUES (null, '{$nombre}', '{$marca}', '{$modelo}', {$precio}, '{$detalles}', {$unidades}, '{$imagen}', '{$eliminado}')";
        if ( $link->query($sql) ) 
        {
            echo 'Producto insertado con ID: '.$link->insert_id;
        }
        else
        {
            echo 'El Producto no pudo ser insertado =(';
        }

        $link->close();
        
    } else echo "Se rechazó el fomulario";

/** SE CREA EL OBJETO DE CONEXION */
?>