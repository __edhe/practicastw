<?php
    // include_once "p03_funciones.php";
?>
<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN'	'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'>
    <head>
        <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
        <title>Práctica 4</title>
        <link rel='stylesheet' href='style.css' type='text/css' media='screen' charset='utf-8'/>
    </head>
    <body>
    <h3>1. Escribir un programa para comprobar si un número es un múltiplo de 5 y 7</h3>
    <form action="p03_funciones.php" method="get">
        <input type="text" name="numero">
        <input type="submit" value="Enviar">
    </form>
    <h3>2. Crear un programa para la generacion repetitiva de 3 números aleatorios hasta obtener una secuencia compuesta por impar, par, impar.</h3>
    <?php //second();?>
    <h3>3. Utiliza un ciclo while para encontrar el primer número entero obtenido aleatoriamente, pero que además sea múltiplo de un número dado.</h3>
    <form action="p03_funciones.php" method="get">
        <input type="text" name="numero2">
        <input type="submit" value="Enviar">
    </form>
    <h3>4. Crear un arreglo cuyos índices van de 97 a 122 y cuyos valores son las letras de la 'a' a la 'z'. Usa la función chr(n) que devuelve el caracter cuyo código ASCII es n para poner el valor en cada índice.</h3>
    <?php //fourth();?>
    <h3>5. Usar las variables $edad y $sexo en una instrucción if para identificar una persona de sexo "femenino", cuya edad oscile entre los 18 y 35 años y mostrar un mensaje de bienvenida apropiado.</h3>
    <form action="p03_funciones.php" method="post">
        <input type="text" name="edad" placeholder="Introducir edad"><br>
        <span>Masculino:</span><input type="radio" name="sexo" id="masculino" value="masculino">
        <span>Femenino:</span><input type="radio" name="sexo" id="femenino" value="femenino">
        <input type="submit" value="Enviar">
    </form>
    <h3>6. Crea en código duro un arreglo asociativo que sirva para registrar el parque vehicular de una ciudad.</h3>
    <form action="p03_funciones.php" method="POST">
        <span>Dejar en blanco para mostrar todo</span><br>
        <input type="text" name="matricula" placeholder="Buscar matricula">
        <input type="submit" value="buscar">
    </form>
    </body>
</html>