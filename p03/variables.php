<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN'	'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='en' lang='en'>
    <head>
        <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
        <title>Page Title</title>
    </head>
    <body>
        <h3>1. Determina cuál de las siguientes variables son válidas y explica por qué</h3>
        <?php 
            $_myvar = "válida";
            $_7var = "válida";
            // _myvar = "Nó válida";
            $myvar = "válida";
            $var7 = "válida";
            $_element1 = "válida";
            // $house*5 = "No válida";
            echo "\$_myvar es $_myvar<br>"."\$_7var es $_7var<br>"."\$myvar es $myvar<br>"."\$var7 es $var7<br>"."\$_element1 es $_element1<br>";
            echo "<p>Todas las anteriores son válidas porque cumplen con la sintaxis de variables, pueden comenzar con guión bajo (_) o con una letra</p>";
            echo "<p><strong>_myvar</strong> no es válida porque una variable se define con \"\$\" y <strong>\$house*5</strong> tampoco es válida ya que contiene un operador (*)</p>";

            //Destructores
            unset($_myvar,$_7var,$myvar,$var7,$_element1);
        ?>
        <h3>2. Proporciona los valores de $a, $b, $c como sigue</h3>
        <?php 
            $a = "ManejadorSQL";
            $b = 'MySQL';
            $c = &$a;

            echo "\$a = $a<br>\$b = $b<br>\$c = $c<br><br>";

            $a = "PHP server";
            $b = &$a;

            echo "\$a = $a<br>\$b = $b<br>\$c = $c<br>";

            unset($a,$b,$c);
        ?>
        <h4>Explicación:</h4>
        <p>En el inciso a) se referenció el valor de $a a $c, es decir, "ManejadorSQL" (valor de $a) fue asignado a la variable $c</p>
        <p>En el inciso b) se cambió el valor de $a por "PHP Server", al referenciar $a a $c, también cambiará el valor de $c por "PHP Server", lo mismo sucede al referenciar el $a a $b, $b tomará el valor de $a</p>
        <h3>3. Verificar la evolución del tipo de estas variables.</h3>
        <?php 
            $a = "PHP5";
            echo $a."&emsp; <strong>Tipo: ".gettype($a)."</strong><br>";
            $z[] = &$a;
            print_r($z); echo "&emsp; <strong>Tipo: ".gettype($z)."</strong><br>";
            $b = "5a version de PHP";
            echo $b."&emsp; <strong>Tipo: ".gettype($b)."</strong><br>";
            settype($b, "integer"); //Daba error
            $c = $b*10;
            echo $c."&emsp; <strong>Tipo: ".gettype($c)."</strong><br>";
            $a .= $b;
            echo $a."&emsp; <strong>Tipo: ".gettype($a)."</strong><br>";
            $b *= $c;
            echo $b."&emsp; <strong>Tipo: ".gettype($b)."</strong><br>";
            $z[0] = "MySQL";
            print_r($z); echo "&emsp; <strong>Tipo: ".gettype($z)."</strong><br>";
            // unset($a,$b,$c,$z);
        ?>
        <h3>4. Obtener variables con $GLOBALS o global</h3>
        <?php 
            function local() {
                $a = 'Hola';
                $b = 'mundo';
                $c = "buenas";
                $z = "tardes";
                echo "Variables locales:<br><br>";
                echo $a ."<br>".$b."<br>".$c."<br>".$z."<br><br>";
                
                global $z; 
                echo "Variables globales:<br><br>";
                echo $GLOBALS['a']."<br>".$GLOBALS['b']."<br>".$GLOBALS['c']."<br>".$z[0]."<br>";
            }   
            local();        
            unset($a,$b,$c,$z);
        ?>
        <h3>5. Dar valor a las variables $a, $b, $c al final del siguiente script.</h3>
        <?php 
            $a = "7 personas";
            $b = (integer) $a;
            $a = "9E3";
            $c = (double) $a;
            // Aquí no sé si el punto es mostrarlo puesto que solo pide asignar valores
            echo $a."<br>".$b."<br>".$c."<br>";

            unset($a,$b,$c);
        ?>
        <h3>6. Dar y comprobar el valor booleano de las variables...</h3>
        <?php 
            $a = "0";
            $b = "TRUE";
            $c = FALSE;
            $d = ($a OR $b);
            $e = ($a AND $c);
            $f = ($a XOR $b);
            
            settype($c, 'integer');
            settype($e, 'integer');

            echo $a."<br>".$b."<br>".$c."<br>".$d."<br>".$e."<br>".$f;
        ?>
        <h4>7. Usando la variable predefinida $_SERVER, determina lo siguiente:</h4>
        <?php 
            echo "Versión de apache y PHP: ".$_SERVER['SERVER_SIGNATURE']."<br>";
            echo "Sistema operativo: ".$_SERVER['SERVER_NAME']."<br>";
            echo "Idioma del cliente: ".$_SERVER['HTTP_ACCEPT_LANGUAGE']."<br>";
        ?>
        <p>
            <a href="http://validator.w3.org/check?uri=referer"><img src="http://www.w3.org/Icons/valid-xhtml10" alt="Valid XHTML 1.0 Strict" height="31" width="88" /></a>
        </p>

    </body>
</html>